#!/bin/bash

master_api_issue_node=$(oc get pod -o wide -n kube-system | grep -v Running | grep master-api | awk '{print $7}')
master_controller_issue_node=$(oc get pod -o wide -n kube-system | grep -v Running | grep master-controllers | awk '{print $7}')
master_etcd_issue_node=$(oc get pod -o wide -n kube-system | grep -v Running | grep master-etcd | awk '{print $7}')
node_issue_node=$(oc get node --no-headers | grep -v Ready | awk '{print $1}')

if [ "$master_api_issue_node" = "" ]
then
	echo -e "\033[32m Master-API: ok \033[0m"
else
	echo -e "\033[31m Master-API:以下节点 master api 服务故障 \033[0m"
	echo $master_api_issue_node | sed "s/ /\n/g"
	echo -e "\033[41;33m请使用root用户逐个登录以上master服务器\033[0m，并使用root用户运行命令 \033[31mmaster-restart api\033[0m 完成重启api服务"
	echo 
fi

if [ "$master_controller_issue_node" = "" ]
then
	echo -e "\033[32m Master-Controller: ok \033[0m"
else
	echo -e "\033[31m Master-Controller:以下节点 master controller 服务故障 \033[0m"
	echo $master_controller_issue_node | sed "s/ /\n/g"
	echo -e "\033[41;33m请使用root用户登录以上master服务器\033[0m，并使用root用户运行命令 \033[31mmaster-restart controllers\033[0m 完成重启controller服务"
	echo 
fi

if [ "$master_etcd_issue_node" = "" ]
then
	echo -e "\033[32m Master-Controller: ok \033[0m"
else
	echo -e "\033[31m Master-etcd:以下节点 master etcd 服务故障 \033[0m"
	echo $master_etcd_issue_node | sed "s/ /\n/g"
	echo -e "\033[41;33m请使用root用户登录以上master服务器\033[0m，并使用root用户运行命令 \033[31mmaster-restart etcd\033[0m 完成重启etcd服务"
	echo 
fi

if [ "$node_issue_node" = "" ]
then
	echo -e "\033[32m Node: ok \033[0m"
else
	echo -e "\033[31m Node:以下节点 Node 服务故障 \033[0m"
	echo $node_issue_node | sed "s/ /\n/g"
	echo -e "\033[41;33m请使用root用户登录以上Node服务器\033[0m，并使用root用户重启 \033[31matomic-openshift-node.service与docker.service服务 \033[0m 完成重启etcd服务"
	echo 
fi

router_issue_pods=$(oc get pod -n default | grep -v Running | grep router | awk '{print $1}')
es_issue_pods=$(oc get pod -n openshift-logging | grep -v Running | grep es | awk '{print $1}')
prometheus_pods=$(oc get pod -n openshift-monitoring | grep -v Running | grep prometheus | awk '{print $1}')
if [ "$router_issue_pods" = "" ]
then
	echo -e "\033[32m Router: ok \033[0m"
else
	echo -e "\033[31m Router: 以下pod不正常，请重启该pod \033[0m"
	echo $router_issue_pods | sed "s/ /\n/g"
	echo -e "请使用root用户运行命令：\033[31moc delete pod $router_issue_pods -n default\033[0m 完成该route pod的重启"
	echo 
fi
if [ "$es_issue_pods" = "" ]
then
	echo -e "\033[32m ES: ok \033[0m"
else
	echo -e "\033[31m ES: 以下pod不正常，请重启该pod \033[0m"
	echo $es_issue_pods | sed "s/ /\n/g"
	echo -e "请使用root用户运行命令：\033[31moc delete pod $es_issue_pods -n openshift-logging\033[0m 完成该es pod的重启"
	echo 
fi
if [ "$prometheus_pods" = "" ]
then
	echo -e "\033[32m Prometheus: ok \033[0m"
else
	echo -e "\033[31m Prometheus: 以下pod不正常，请重启该pod \033[0m"
	echo $prometheus_pods | sed "s/ /\n/g"
	echo -e "请使用root用户运行命令：\033[31moc delete pod $prometheus_pods -n openshift-monitoring\033[0m 完成该prometheus pod的重启"
	echo 
fi

